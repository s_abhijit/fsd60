package com.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.EmployeeDao;
import com.dto.Employee;

@WebServlet("/GetEmployeeByName")
public class GetEmployeeByName extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 response.setContentType("text/html");
			PrintWriter out = response.getWriter();

			String empName =request.getParameter("empName");
			EmployeeDao employeeDao = new EmployeeDao();
			Employee employee = employeeDao.empName(empName);
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/HRHome");
			requestDispatcher.include(request, response);
//			System.out.println(employee);
			 
			if (employee != null) {
				
	             out.print("<table border='2'>");
				
				out.print("<tr>");
				out.print("<th> EmpId    </th>");
				out.print("<th> EmpName  </th>");
				out.print("<th> Salary   </th>");
				out.print("<th> Gender   </th>");
				out.print("<th> Email-Id </th>");
				out.print("<th> Password </th>");
				out.print("</tr>");
				
				out.print("<tr>");
				out.print("<td>" + employee.getEmpId()    + "</td>");
				out.print("<td>" + employee.getEmpName()  + "</td>");
				out.print("<td>" + employee.getSalary()   + "</td>");
				out.print("<td>" + employee.getGender()   + "</td>");
				out.print("<td>" + employee.getEmailId()  + "</td>");
				out.print("<td>" + employee.getPassword() + "</td>");
				out.print("</tr>");		
				out.print("</table>");
			}
						
			
				
			 else {
				
				
				out.print("<h1 style='color:red'>Record Not Found</h1>");
			}

		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		doGet(request, response);
	}

}